const {isUnderage} = require('../src/isUnderage.js')

test('isUnderage', () => {
  isUnderage(new Date('01-01-2005'))
  expect(isUnderage(new Date('01-01-1987'))).toBe(false)
})
