const moment = require('moment')

const UNDERAGE_LIMIT = 18

const isUnderage = (birthday) => {
  const today = moment()
  const birthdayDate = moment(birthday)

  if (today.diff(birthdayDate, 'years') >= UNDERAGE_LIMIT) {
    return false
  }

  return true
}

exports.isUnderage = isUnderage
